import React, {Component} from 'react';

import logo from './images/logo.png'
import bg from './images/background.png'
import bg_sm from './images/background-sm.png'
//components
import Search from "./Search";


class App extends Component {
  render() {
    return (
        <div className="container">
            <div className="row">
                <div className="mx-md-auto">
                    <img src={logo} alt="logo" className="logo" width="100px" height="100px"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-7">
                    <img src={bg} alt="background" className="bg-image d-none d-md-block"/>
                </div>
                <div className="col-md-5">
                    <Search/>
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <img src={bg_sm} alt="background for small devices" className="bg-sm d-md-none"/>
                </div>
            </div>
        </div>
    );
  }
}

export default App;
