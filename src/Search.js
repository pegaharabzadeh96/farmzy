import React, {Component} from 'react';
import 'font-awesome/css/font-awesome.min.css'

class Search extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col text-right" dir="rtl">
                        <h4 className="mt-5">پلتفرم هوشمند جستجوی گوسفند</h4>
                        <h1 className="mt-3">
                            <b>گوسفند خودتُ پیدا کن!</b>
                        </h1>
                    </div>
                </div>
                <div className="row" >
                    <div className="input-group input-group-lg mt-3">
                        <div className="input-group-prepend">
                            <button className="btn btn-primary">جستجو</button>
                        </div>
                        <input type="text" className="form-control searcher" placeholder="&#xf041;" dir="rtl"/>
                        <i className="fa fa-map-marker mt-3"/>
                    </div>
                   <div className="col text-right mt-3" dir="rtl">
                       <h6>
                          <b>
                              به کمک فارم زی،به سادگی از بهترین دامداران منطقه خود گوسفند زنده بخرید و از خدمات ارسال رایگان به همراه قصاب بهره مند شوید.
                          </b>
                       </h6>
                   </div>
                </div>

            </div>
        );
    }
}

export default Search;